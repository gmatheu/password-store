#!/usr/bin/env bash

test_description='Test show'
cd "$(dirname "$0")"
. ./setup.sh

test_expect_success 'Test "show" command' '
	"$PASS" init $KEY1 &&
	"$PASS" generate cred1 20 &&
	"$PASS" show cred1
'

test_expect_success 'Test "show" command with spaces' '
	"$PASS" insert -e "I am a cred with lots of spaces"<<<"BLAH!!" &&
	[[ $("$PASS" show "I am a cred with lots of spaces") == "BLAH!!" ]]
'

test_expect_success 'Test "show" of nonexistant password' '
	test_must_fail "$PASS" show cred2
'

test_expect_success 'Show list as a tree' '
	"$PASS" insert -e "a/1"<<<"a1" &&
	"$PASS" insert -e "a/2"<<<"a2" &&
	"$PASS" insert -e "b"<<<"b" &&
	"$PASS" list | grep -q ".*a" &&
	"$PASS" list | grep -q ".*1" &&
	"$PASS" list | grep -q ".*b"
'

test_expect_success 'Show plain list' '
	"$PASS" insert -e "a/1"<<<"a1" &&
	"$PASS" insert -e "a/2"<<<"a2" &&
	"$PASS" insert -e "b"<<<"b" &&
	"$PASS" list --plain | grep -q "a/1" &&
	"$PASS" list --plain | grep -q "a/2" &&
	"$PASS" list --plain | grep -q "b"
'

test_expect_success 'Show plain list on path' '
	"$PASS" insert -e "a/1"<<<"a1" &&
	"$PASS" insert -e "a/2"<<<"a2" &&
	"$PASS" insert -e "b"<<<"b" &&
	"$PASS" list --plain a | grep -q "a/1" &&
	"$PASS" list --plain a | grep -q "a/2" &&
	"$PASS" list --plain a | grep -c "a/" | grep "2" &&
	"$PASS" list --plain a | grep -c "b" | grep "0"
'

test_expect_success 'Shows header by default' '
	"$PASS" insert -e "a/1"<<<"a1" &&
	"$PASS" list | grep "Password Store"
'

test_expect_success 'Do not show header' '
	"$PASS" insert -e "a/1"<<<"a1" &&
	"$PASS" list --no-header | grep -c "Password Store" | grep "0"
'

test_done
